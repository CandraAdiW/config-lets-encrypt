FROM node:20-alpine As development


WORKDIR /home/nestjs

COPY package*.json ./

#RUN npm install glob rimraf

#RUN npm install npm@latest -g

RUN npm install

COPY . .
# COPY ./src ./
# COPY ./test ./
# COPY ./.env ./
# COPY ./nest-cli.json ./
# COPY ./tsconfig.json ./
# COPY ./tsconfig.build.json ./

RUN npm run build

RUN ls -al /home/nestjs/

RUN whoami

# RUN chown node:node -R /home/nestjs

# RUN ls -al /home/nestjs/

# RUN whoami

# FROM node:20-alpine as production

# ARG NODE_ENV=production
# ENV NODE_ENV=${NODE_ENV}

# WORKDIR /usr/src/app

# COPY package*.json ./

# RUN npm install --only=production

# COPY . .

# COPY --from=development /usr/src/app/dist ./dist

# CMD ["node", "dist/main"]


